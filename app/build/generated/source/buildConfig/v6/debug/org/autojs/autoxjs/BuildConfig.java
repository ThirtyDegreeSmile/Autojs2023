/**
 * Automatically generated file. DO NOT MODIFY
 */
package org.autojs.autoxjs;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "org.autojs.autoxjs";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "v6";
  public static final int VERSION_CODE = 638;
  public static final String VERSION_NAME = "6.3.8";
  // Field from product flavor: v6
  public static final String CHANNEL = "v6";
  // Field from default config.
  public static final boolean isMarket = false;
}
